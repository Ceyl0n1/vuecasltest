import Vue from "vue";
import Router from "vue-router";

Vue.use(Router);

export default new Router({
  routes: [
    {
      name: "index",
      path: "/",
      component: () => import("@/views/index")
    },
    {
      name: "about",
      path: "/about",
      component: () => import("@/views/about")
    },
    {
      name: "secret",
      path: "/secret",
      component: () => import("@/views/secret")
    },
  ]
});
