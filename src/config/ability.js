import { AbilityBuilder, Ability } from '@casl/ability'

// const { can, rules } = new AbilityBuilder();

// can('val-1', 'val-1-2');
// can('getCustomer');
// can('updateEmployees');
// console.log('rules', rules)

const { rules } = new AbilityBuilder();

export default new Ability(rules);
