import Vue from 'vue'
import Vuex from 'vuex'

import ability from '@/config/ability'

Vue.use(Vuex)

export const store = new Vuex.Store({
  state() {
    return {
      language: 'en',
    }
  },

  getters: {
    getLanguage: state => state.language,
  },

  mutations: {
    SET_LANGUAGE(state, value) {
      state.language = value;
    },
  },

  actions: {
    setLocale({ commit }, locale = "ru") {
      commit('SET_LANGUAGE', locale);
    },

    login(context) {
      console.log('actions login', context)

      // Получаем токен

      // получаем список разрешеных методов для пользователя
      const arr = [
        { action: 'updateEmployees' },
        { action: 'getEmployees' },
      ];

      ability.update(arr)

      console.log('ability', ability)
    },
  }
})
