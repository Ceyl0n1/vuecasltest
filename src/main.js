import Vue from 'vue'
import App from './App.vue'
import router from './router'
import { store } from './store'

import ability from './config/ability'
import { abilitiesPlugin } from '@casl/vue'

Vue.config.productionTip = false

// правила по умолчанию
Vue.use(abilitiesPlugin, ability)

var vm = new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app')

global.vm = vm;
